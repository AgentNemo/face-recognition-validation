#!/bin/bash
set -v
export ROOT=$(cd $(dirname $0)/.. && pwd)

# 1. Install Python, pip
apt-get update
apt-get install -y python-pip python-dev swig python-numpy

# installing dlib
apt-get install -y build-essential cmake pkg-config
apt-get install -y libx11-dev libatlas-base-dev
apt-get install -y libjpeg62-turbo-dev
apt-get install -y libgtk-3-dev libboost-python-dev
apt-get install -y python-dev python-pip python3-dev python3-pip
pip2 install -y -U pip numpy
pip3 install -y -U pip numpy

wget http://dlib.net/files/dlib-19.16.tar.bz2
tar xvf dlib-19.16.tar.bz2
cd dlib-19.16/
mkdir build
cd build
cmake ..
cmake --build . --config Release
make install
ldconfig