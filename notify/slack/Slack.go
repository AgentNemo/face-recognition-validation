package slack

import (
	"XYZ/logger"
	"fmt"
	slacky "github.com/slack-go/slack"
)

type Slack struct {
	Client   *slacky.Client
	Channels []string
}

func New(token string, channels ...string) *Slack {
	client := slacky.New(token)
	return &Slack{Client: client, Channels: channels}
}

func (s Slack) Send(status string, msg string, keysAndValues ...interface{}) {
	for _, channel := range s.Channels {
		s.sendChannel(status, msg, channel, keysAndValues...)
	}
}

func (s Slack) sendChannel(status string, msg string, channelID string, keysAndValues ...interface{}) error {
	context := make([]slacky.AttachmentField, 0)
	for i := 0; i < len(keysAndValues); i = i + 2 {
		field := slacky.AttachmentField{}
		field.Title = keysAndValues[i].(string)
		if i+1 < len(keysAndValues) {
			field.Value = keysAndValues[i+1].(string)
		}
		context = append(context, field)
	}
	attachment := slacky.Attachment{
		Pretext: msg,
		Fields:  context,
	}
	channelID, _, err := s.Client.PostMessage(
		channelID,
		slacky.MsgOptionText(fmt.Sprintf("Status: %s", status), false),
		slacky.MsgOptionAttachments(attachment),
		slacky.MsgOptionAsUser(true),
	)
	if err != nil {
		logger.LogF("Slack: failed to send to %s", logger.DEBUG, channelID)
		return err
	}
	logger.LogF("Slack: successful send to %s", logger.DEBUG, channelID)
	return nil
}
