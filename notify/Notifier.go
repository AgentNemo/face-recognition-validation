package notify

// Interface for notifying
type Notifier interface {
	Send(status string, msg string, keysAndValues ...interface{}) // Sends a notification
}
