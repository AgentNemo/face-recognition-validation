package messenger

import (
	"github.com/joomcode/errorx"
)

var (
	ErrorMessenger                = errorx.NewNamespace("Messengers")
	ErrorMessengerInitArguments   = errorx.NewType(ErrorMessenger, "Arguments")
	ErrorMessengerInitCredentials = errorx.NewType(ErrorMessenger, "Credentials")
	ErrorMessengerInitConversion  = errorx.NewType(ErrorMessenger, "Conversion")
	ErrorMessengerPhoto           = errorx.NewType(ErrorMessenger, "Photo")
	ErrorMessengerText            = errorx.NewType(ErrorMessenger, "Text")
	ErrorMessengerNotAllowed      = errorx.NewType(ErrorMessenger, "NotAllowed")
	ErrorMessengerSend            = errorx.NewType(ErrorMessenger, "Send")
)

type Permission string

const (
	PermissionTextReceive  Permission = "TextReceive"
	PermissionPhotoReceive Permission = "PhotoReceive"
	PermissionTextSend     Permission = "TextSend"
	PermissionPhotoSend    Permission = "PhotoSend"
	PermissionStatistic    Permission = "Statistic"
	PermissionAdmin        Permission = "Admin"
	PermissionOnUserJoined Permission = "OnUserJoined"
	PermissionReminder     Permission = "Reminder"
)

// Interface for Messenger Apps
type Messenger interface {
	GetID() string                          // Messenger unique id
	Start(args ...interface{})              // Start the Messenger; optional add args
	TextPull() (Text, error)                // Pull one Text
	PhotoPull() (Photo, error)              // Pull a Photo
	TextReceive(func(text Text)) error      // Adds a function that is triggered when a new Text arrive
	PhotoReceive(func(photo Photo)) error   // Adds a function that is triggered when a new Photo arrive
	PhotoSend(Photo) error                  // Sends a Photo
	TextSend(Text) error                    // Sends a Text
	NewText(text string, chat Chat) Text    // Creates a new Text
	NewPhoto(bytes []byte, chat Chat) Photo // Creates a new Photo
}

// Interface for Sender Information
type Sender interface {
	Name() string // Get Sender information
	ID() string   // unique Sender id
}

// Interface for Chat Information
type Chat interface {
	Name() string // Get Chat information
	ID() string   // unique Chat id
}

// Interface for Text
type Text interface {
	Text() string   // Get Text
	Sender() Sender // From whom is the Text
	Chat() Chat     // From which Chat was the Text send
}

// Interface for Photos
type Photo interface {
	Photo() []byte  // Get Photo
	Sender() Sender // From whom is the Photo
	Chat() Chat     // From which Chat was the Photo send
}
