package messenger

import "fmt"

type Command string

const (
	CommandSendStatistic             Command = "/stats"
	CommandDetectFacesMode           Command = "/detect"
	CommandValidateFaceMode          Command = "/validate"
	CommandSaveSampleMode            Command = "/sampleSave"
	CommandSavePhotoMode             Command = "/photoSave"
	CommandApplySamples              Command = "/samplesApply"
	CommandDownloadPhotos            Command = "/download"
	CommandDetectFacesSavingDataMode Command = "/detectSaving"
	CommandHelp                      Command = "/help"

	// generated by the messenger
	CommandUserJoined Command = "/userJoined"
)

func CommandGetHelp() string {
	return fmt.Sprintf(
		"%s\n"+
			"%s\n"+
			"%s\n"+
			"%s\n"+
			"%s\n"+
			"%s\n"+
			"%s\n"+
			"%s\n",
		CommandSendStatistic,
		CommandDetectFacesMode,
		CommandValidateFaceMode,
		CommandSaveSampleMode,
		CommandSavePhotoMode,
		CommandApplySamples,
		CommandDownloadPhotos,
		CommandDetectFacesSavingDataMode)
}
