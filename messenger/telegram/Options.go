package telegram

import (
	"XYZ/db"
	"XYZ/db/models"
	"XYZ/logger"
	"XYZ/messenger"
	"bytes"
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
	"time"
)

// TODO: add an interface
type OptionsTelegram struct {
	permissions      []messenger.Permission
	TextReceive      func(text messenger.Text)
	PhotoReceive     func(photo messenger.Photo)
	TextSend         func(text messenger.Text) error
	PhotoSend        func(photo messenger.Photo) error
	Reminder         func()
	WelcomeMessage   string
	ReminderMessage  string
	ReminderInterval time.Duration
	Admin            string
}

func NewOptions(permissions []messenger.Permission, admin string) *OptionsTelegram {
	return &OptionsTelegram{permissions: permissions, Admin: admin}
}

func (o *OptionsTelegram) Permissions() []messenger.Permission {
	return o.permissions
}

func (o *OptionsTelegram) Handle(telegram *Telegram) {
	logger.LogF("Telegram: handling for permissions %s", logger.INFO, o.Permissions())

	if o.inPermissions(messenger.PermissionTextReceive) {
		o.TextReceive = o.defaultTextReceive(telegram)
		telegram.Bot.Handle(tb.OnText, func(m *tb.Message) {
			o.TextReceive(&Text{
				text:   m.Text,
				sender: Sender{*m.Sender},
				chat:   Chat{*m.Chat},
			})
		})
	}

	if o.inPermissions(messenger.PermissionPhotoReceive) {
		o.PhotoReceive = o.defaultPhotoReceive(telegram)
		telegram.Bot.Handle(tb.OnPhoto, func(m *tb.Message) {
			buf := new(bytes.Buffer)
			reader, err := telegram.Bot.GetFile(m.Photo.MediaFile())
			if err != nil {
				logger.Log("Telegram: photo could not be processed", logger.DEBUG)
				return
			}
			_, err = buf.ReadFrom(reader)
			if err != nil {
				logger.Log("Telegram: photo could not be read", logger.DEBUG)
				return
			}

			sender := Sender{*m.Sender}
			chat := Chat{*m.Chat}

			o.PhotoReceive(&Photo{
				photo:  buf.Bytes(),
				sender: sender,
				chat:   chat,
			})
		})
	}

	if o.inPermissions(messenger.PermissionTextSend) {
		o.TextSend = o.defaultTextSend(telegram)
	}

	if o.inPermissions(messenger.PermissionPhotoSend) {
		o.PhotoSend = o.defaultPhotoSend(telegram)
	}

	if o.inPermissions(messenger.PermissionStatistic) {
		// Add statistic
	}

	if o.inPermissions(messenger.PermissionAdmin) {
		// wrap onText in admin check
		telegram.Bot.Handle(tb.OnText, func(m *tb.Message) {
			if m.Text[0] != '/' {
				return
			}
			if m.Sender.Username != o.Admin {
				logger.LogF("Telegram: %s is not admin", logger.DEBUG, m.Sender.FirstName)
				return
			}
			o.TextReceive(&Text{
				text:   m.Text,
				sender: Sender{*m.Sender},
				chat:   Chat{*m.Chat},
			})
		})
	}

	if o.inPermissions(messenger.PermissionOnUserJoined) {
		telegram.Bot.Handle(tb.OnUserJoined, func(m *tb.Message) {
			o.TextReceive(&Text{
				text:   string(messenger.CommandUserJoined),
				sender: Sender{*m.UserJoined},
				chat:   Chat{*m.Chat},
			})
			message := fmt.Sprintf(o.WelcomeMessage, m.UserJoined.FirstName)
			telegram.TextSend(NewText(message, Sender{*m.UserJoined}, Chat{*m.Chat}))
		})
	}

	if o.inPermissions(messenger.PermissionReminder) {
		go func() {
			max := 30
			for {
				if o.Reminder == nil {
					logger.Log("Telegram: reminder func not provided", logger.DEBUG)
					max--
					time.Sleep(time.Second)
					if max == 0 {
						logger.Log("Telegram: reminder timeout threshold reached", logger.DEBUG)
						return
					}
					continue
				}
				logger.Log("Telegram: start reminder", logger.INFO)
				o.Reminder()
				time.Sleep(o.ReminderInterval)
			}
		}()
	}

}

func (o *OptionsTelegram) inPermissions(permission messenger.Permission) bool {
	for _, p := range o.Permissions() {
		if p == permission {
			return true
		}
	}
	return false
}

func (o *OptionsTelegram) defaultPhotoReceive(telegram *Telegram) func(photo messenger.Photo) {
	return func(photo messenger.Photo) {
		telegram.QueuePhoto = append(telegram.QueuePhoto, photo)
		logger.Log("Telegram: added new photo to the queue", logger.DEBUG)
	}
}

func (o *OptionsTelegram) defaultTextReceive(telegram *Telegram) func(text messenger.Text) {
	return func(text messenger.Text) {
		telegram.QueueText = append(telegram.QueueText, text)
		logger.Log("Telegram: added new text to the queue", logger.DEBUG)

	}
}

func (o *OptionsTelegram) defaultTextSend(telegram *Telegram) func(text messenger.Text) error {
	return func(text messenger.Text) error {
		chat := text.Chat().(Chat)
		_, err := telegram.Bot.Send(&chat, text.Text())
		if err != nil {
			logger.Log("Telegram: problem while sending text", logger.DEBUG)
			return messenger.ErrorMessengerSend.WrapWithNoMessage(err)
		}
		return nil
	}
}

func (o *OptionsTelegram) defaultPhotoSend(telegram *Telegram) func(photo messenger.Photo) error {
	return func(photo messenger.Photo) error {
		chat := photo.Chat().(Chat)
		_, err := telegram.Bot.Send(&chat, &tb.Photo{File: tb.FromReader(bytes.NewReader(photo.Photo()))})
		if err != nil {
			logger.Log("Telegram: problem while sending photo", logger.DEBUG)
			return messenger.ErrorMessengerSend.WrapWithNoMessage(err)
		}
		return nil
	}
}

func (o *OptionsTelegram) defaultReminder(telegram *Telegram, db db.DB) {
	// TODO: outsource into statistic struct
	o.Reminder = func() {
		chats := db.GetChats(models.Messenger{
			Name: telegram.GetID(),
		})
		for _, c := range chats {
			go func(chat models.Chat) {
				tbChat, err := telegram.Bot.ChatByID(chat.UID)
				if err != nil {
					logger.LogF("Telegram: could not find chat %s", logger.INFO, chat.UID)
					return
				}
				telegram.TextSend(telegram.NewText(o.buildReminder(chat.Name, 0), Chat{*tbChat}))
				logger.LogF("Telegram: reminder send to chat %s", logger.INFO, chat.Name)
			}(c)
		}
	}
}

func (o *OptionsTelegram) buildReminder(values ...interface{}) string {
	return fmt.Sprintf(o.ReminderMessage, values...)
}
