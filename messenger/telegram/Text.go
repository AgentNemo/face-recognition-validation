package telegram

import (
	"XYZ/messenger"
)

type Text struct {
	text   string
	sender Sender
	chat   Chat
}

func NewText(text string, sender messenger.Sender, chat messenger.Chat) messenger.Text {
	var tSender Sender
	tSender.FirstName = ""
	if sender != nil {
		tSender = sender.(Sender)
	}
	return &Text{
		text:   text,
		sender: tSender,
		chat:   chat.(Chat),
	}
}

func (c *Text) Text() string {
	return c.text
}

func (c *Text) Sender() messenger.Sender {
	return c.sender
}

func (c *Text) Chat() messenger.Chat {
	return c.chat
}
