package telegram

import (
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
)

type Chat struct {
	tb.Chat
}

func (c Chat) Name() string {
	return c.Title
}

func (c Chat) ID() string {
	return fmt.Sprint(c.Chat.ID)
}
