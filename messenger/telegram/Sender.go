package telegram

import (
	tb "gopkg.in/tucnak/telebot.v2"
	"strconv"
)

type Sender struct {
	tb.User
}

func (s Sender) Name() string {
	return s.FirstName
}

func (s Sender) ID() string {
	return strconv.Itoa(s.User.ID)
}
