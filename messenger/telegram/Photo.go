package telegram

import (
	"XYZ/messenger"
)

type Photo struct {
	photo  []byte
	sender Sender
	chat   Chat
}

func NewPhoto(bytes []byte, sender messenger.Sender, chat messenger.Chat) messenger.Photo {
	var tSender Sender
	tSender.FirstName = ""
	if sender != nil {
		tSender = sender.(Sender)
	}
	return &Photo{
		photo:  bytes,
		sender: tSender,
		chat:   chat.(Chat),
	}
}

func (p *Photo) Photo() []byte {
	return p.photo
}

func (p *Photo) Sender() messenger.Sender {
	return p.sender
}

func (p *Photo) Chat() messenger.Chat {
	return p.chat
}
