package telegram

import (
	"XYZ/config"
	"XYZ/db"
	"XYZ/logger"
	"XYZ/messenger"
	"crypto/md5"
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
	"io/ioutil"
	"time"
)

type Telegram struct {
	ID         string
	Bot        *tb.Bot
	QueuePhoto []messenger.Photo
	QueueText  []messenger.Text
	Options    *OptionsTelegram
}

func NewByConfig(config config.Messenger) (*Telegram, error) {
	permissions := make([]messenger.Permission, 0)
	for _, permission := range config.Permissions {
		permissions = append(permissions, messenger.Permission(permission))
	}
	options := NewOptions(permissions, config.Admin)
	welcomeMessage, err := ioutil.ReadFile(config.WelcomeMessage)
	if err == nil {
		options.WelcomeMessage = string(welcomeMessage)
	}
	reminderMessage, err := ioutil.ReadFile(config.ReminderMessage)
	if err == nil {
		options.ReminderMessage = string(reminderMessage)
	}
	interval, err := time.ParseDuration(config.ReminderInterval)
	if err != nil {
		options.ReminderInterval, _ = time.ParseDuration("24h")
	} else {
		options.ReminderInterval = interval
	}
	return New(options, config.Credentials)
}

func New(args ...interface{}) (*Telegram, error) {
	t := Telegram{}
	t.QueuePhoto = make([]messenger.Photo, 0)
	t.QueueText = make([]messenger.Text, 0)
	t.Options = NewOptions([]messenger.Permission{}, "")

	index := 0
	switch len(args) {
	case 2:
		options, ok := args[index].(*OptionsTelegram)
		if ok {
			index++
			t.Options = options
			logger.Log("Telegram: options found", logger.DEBUG)
		}
		fallthrough
	case 1:
		token, ok := args[index].(string)
		if !ok {
			logger.Log(fmt.Sprintf("Telegram: %s could not be converted to a token", args[index]), logger.DEBUG)
			return nil, messenger.ErrorMessengerInitConversion.New(fmt.Sprintf("%s", args[index]))
		}
		bot, err := tb.NewBot(tb.Settings{
			Token:  token,
			Poller: &tb.LongPoller{Timeout: 10 * time.Second},
			Reporter: func(err error) {
				//logger.LogE(err)
			},
		})
		if err != nil {
			logger.Log(fmt.Sprintf("Telegram: could not initialize with token %s", token), logger.DEBUG)
			return nil, messenger.ErrorMessengerInitCredentials.New(token)
		}

		t.Bot = bot
		t.ID = fmt.Sprintf("%x", md5.Sum([]byte(fmt.Sprintf("%s%s", "Telegram", token))))
		logger.LogF("Telegram: telegram created with id %s", logger.DEBUG, t.ID)
		// Apply options
		t.Options.Handle(&t)
		return &t, nil
	default:
		return nil, messenger.ErrorMessengerInitArguments.New("wrong number of arguments")
	}
}

func (t *Telegram) GetID() string {
	return t.ID
}

func (t *Telegram) NewText(text string, chat messenger.Chat) messenger.Text {
	return NewText(text, nil, chat)
}

func (t *Telegram) NewPhoto(bytes []byte, chat messenger.Chat) messenger.Photo {
	return NewPhoto(bytes, nil, chat)
}

func (t *Telegram) Start(args ...interface{}) {
	switch len(args) {
	case 1:
		db, ok := args[0].(db.DB)
		if ok {
			t.Options.defaultReminder(t, db)
		}
	}
	go func() {
		t.Bot.Start()
	}()
}

func (t *Telegram) TextPull() (messenger.Text, error) {
	if len(t.QueueText) < 1 {
		return nil, messenger.ErrorMessengerText.NewWithNoMessage()
	}
	text := t.QueueText[0]
	t.QueueText = t.QueueText[1:]
	return text, nil
}

func (t *Telegram) PhotoPull() (messenger.Photo, error) {
	if len(t.QueuePhoto) < 1 {
		return nil, messenger.ErrorMessengerPhoto.NewWithNoMessage()
	}
	photo := t.QueuePhoto[0]
	t.QueuePhoto = t.QueuePhoto[1:]
	return photo, nil
}

func (t *Telegram) TextReceive(f func(text messenger.Text)) error {
	if t.Options.TextReceive == nil {
		return messenger.ErrorMessengerNotAllowed.NewWithNoMessage()
	}
	t.Options.TextReceive = f
	return nil
}

func (t *Telegram) PhotoReceive(f func(photo messenger.Photo)) error {
	if t.Options.PhotoReceive == nil {
		return messenger.ErrorMessengerNotAllowed.NewWithNoMessage()
	}
	t.Options.PhotoReceive = f
	return nil
}

func (t *Telegram) PhotoSend(photo messenger.Photo) error {
	if t.Options.PhotoSend == nil {
		return messenger.ErrorMessengerNotAllowed.NewWithNoMessage()
	}
	return t.Options.PhotoSend(photo)
}

func (t *Telegram) TextSend(text messenger.Text) error {
	if t.Options.TextSend == nil {
		return messenger.ErrorMessengerNotAllowed.NewWithNoMessage()
	}
	return t.Options.TextSend(text)
}
