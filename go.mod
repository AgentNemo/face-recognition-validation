module gitlab.com/AgentNemo/face-recognition-validation

go 1.15

require (
	github.com/Kagami/go-face v0.0.0-20200825065730-3dd2d74dccfb
	github.com/joomcode/errorx v1.0.3
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/nakabonne/gosivy v0.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/slack-go/slack v0.8.0
	go.uber.org/zap v1.16.0
	golang.org/x/oauth2 v0.0.0-20201109201403-9fd604954f58
	google.golang.org/api v0.36.0
	gopkg.in/tucnak/telebot.v2 v2.3.5
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.20.12
)
