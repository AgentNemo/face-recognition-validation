package main

import (
	"XYZ/config"
	"XYZ/db"
	"XYZ/db/models"
	"XYZ/db/sqlite"
	"XYZ/detection"
	"XYZ/logger"
	"XYZ/messenger"
	"XYZ/messenger/telegram"
	"XYZ/notify"
	slack2 "XYZ/notify/slack"
	"XYZ/storage"
	"XYZ/storage/googledrive"
	"fmt"
	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
	_ "image/jpeg"
	"log"
)

var (
	ApplicationConfig = "./config/application.json"
	MessengerConfig   = "./config/messenger.json"
	StorageConfig     = "./config/storage.json"
	NotifierConfig    = "./config/notifier.json"
	DB_PATH           = "./sqlite.db"
)

type Mode string

const (
	PhotoModeFaceDetection           Mode = "FaceDetection"           // face detection
	PhotoModeFaceValidation          Mode = "FaceValidation"          // photos validation
	PhotoModeFaceSavingData          Mode = "FaceSavingData"          // save detected photos data
	PhotoModeFaceSavingPhoto         Mode = "FaceSavingPhoto"         // save images
	PhotoModeFaceDetectionSavingData Mode = "FaceDetectionSavingData" // detect images and save data
)

type Application struct {
	Messengers messenger.Messenger
	Detections detection.Detection
	Storages   storage.Storage
	Notifiers  []notify.Notifier
	DB         db.DB
	Mode       Mode
}

func NewByConfig(path string) (*Application, error) {
	config, err := config.UnmarshalConfigApplication(path)
	if err != nil {
		return nil, err
	}
	err = logger.New(config.LogLevel, config.LogFiles...)
	if err != nil {
		return nil, err
	}
	application := Application{Mode: Mode(config.ModeDefault)}
	return &application, nil
}

func (application *Application) HandleCommands(text messenger.Text) error {
	logger.LogF("Command: handle %s", logger.DEBUG, text.Text())
	// Detect commands
	switch messenger.Command(text.Text()) {
	case messenger.CommandHelp:
		return application.Messengers.TextSend(application.Messengers.NewText(messenger.CommandGetHelp(), text.Chat()))
	case messenger.CommandSendStatistic:
		// Storage statistic
		data := application.Storages.StatisticDataAll()
		ret := "Storage Statistic\n"
		for username, d := range data {
			ret += fmt.Sprintf("%s: %d\n", username, d)
		}
		application.Messengers.TextSend(application.Messengers.NewText(ret, text.Chat()))
	case messenger.CommandDetectFacesMode:
		// Just face detection
		application.Mode = PhotoModeFaceDetection
		return application.Messengers.TextSend(application.Messengers.NewText("Mode: face detection", text.Chat()))
	case messenger.CommandValidateFaceMode:
		// Face validation
		application.Mode = PhotoModeFaceValidation
		return application.Messengers.TextSend(application.Messengers.NewText("Mode: face validation", text.Chat()))
	case messenger.CommandSaveSampleMode:
		// Face saving
		application.Mode = PhotoModeFaceSavingData
		return application.Messengers.TextSend(application.Messengers.NewText("Mode: sample save", text.Chat()))
	case messenger.CommandSavePhotoMode:
		// Face saving
		application.Mode = PhotoModeFaceSavingPhoto
		return application.Messengers.TextSend(application.Messengers.NewText("Mode: photo save", text.Chat()))
	case messenger.CommandDetectFacesSavingDataMode:
		application.Mode = PhotoModeFaceDetectionSavingData
		return application.Messengers.TextSend(application.Messengers.NewText("Mode: face detection and sample save", text.Chat()))
	case messenger.CommandApplySamples:
		data, err := application.Storages.DownloadDataAll()
		if err != nil {
			logger.Log("Application: error while downloading storage data", logger.DEBUG)
			application.Messengers.TextSend(application.Messengers.NewText("Error while downloading", text.Chat()))
		}
		for username, photos := range data {
			for _, photo := range photos {
				application.Detections.AddSamples(username, photo)
			}
		}
		application.Detections.Apply()
		logger.Log("All user samples added", logger.DEBUG)
	case messenger.CommandDownloadPhotos:
		// Fetch photos
		photos, err := application.Storages.DownloadImg(text.Sender().Name())
		if err != nil {
			application.Messengers.TextSend(application.Messengers.NewText("Error while downloading photos", text.Chat()))
		}
		for _, photo := range photos {
			application.Messengers.PhotoSend(application.Messengers.NewPhoto(photo, text.Chat()))
		}
	case messenger.CommandUserJoined:

	}
	return nil
}

func (application *Application) HandlePhotos(photo messenger.Photo) error {
	fmt.Println(photo.Chat().ID())
	chat := models.Chat{
		UID:  photo.Chat().ID(),
		Name: photo.Chat().Name(),
	}
	user, _ := application.DB.GetUser(chat, models.User{
		Name: photo.Sender().Name(),
	})
	user.CountDetect++
	application.DB.AddUser(models.Messenger{
		Name: application.Messengers.GetID(),
	}, chat, user)
	switch application.Mode {
	case PhotoModeFaceDetection:
		// do detection and send the results back
		faces, err := application.Detections.Detect(photo.Photo())
		if err != nil {
			logger.LogE(err)
			application.Messengers.TextSend(application.Messengers.NewText("Error while detection faces", photo.Chat()))
		}
		if len(faces) < 1 {
			application.Messengers.TextSend(application.Messengers.NewText("No faces detected", photo.Chat()))
		}
		for _, face := range faces {
			application.Messengers.PhotoSend(application.Messengers.NewPhoto(face, photo.Chat()))
		}
	case PhotoModeFaceSavingData:
		data, err := application.Detections.Raw(photo.Photo())
		if err != nil {
			logger.LogE(err)
			application.Messengers.TextSend(application.Messengers.NewText("Error while detection faces", photo.Chat()))
		}
		// just storage the given photo
		return application.Storages.UploadData(data, photo.Sender().Name())
	case PhotoModeFaceValidation:
		// validate photo
		usernames, err := application.Detections.Recognize(photo.Photo())
		if err != nil {
			logger.LogE(err)
			application.Messengers.TextSend(application.Messengers.NewText("Error while validating faces", photo.Chat()))
		}
		for _, username := range usernames {
			application.Messengers.TextSend(application.Messengers.NewText(username, photo.Chat()))
		}
	case PhotoModeFaceSavingPhoto:
		photos, err := application.Detections.Detect(photo.Photo())
		if err != nil {
			logger.LogE(err)
			application.Messengers.TextSend(application.Messengers.NewText("Error while detection faces", photo.Chat()))
		}
		for _, p := range photos {
			application.Storages.UploadData(p, photo.Sender().Name())

		}
	case PhotoModeFaceDetectionSavingData:
		faces, err := application.Detections.Detect(photo.Photo())
		if err != nil {
			logger.LogE(err)
			application.Messengers.TextSend(application.Messengers.NewText("Error while detection faces", photo.Chat()))
		}
		if len(faces) < 1 {
			application.Messengers.TextSend(application.Messengers.NewText("No faces detected", photo.Chat()))
			return nil
		}
		for _, face := range faces {
			application.Messengers.PhotoSend(application.Messengers.NewPhoto(face, photo.Chat()))
		}

		data, err := application.Detections.Raw(photo.Photo())
		if err != nil {
			logger.LogE(err)
			application.Messengers.TextSend(application.Messengers.NewText("Error while detection faces", photo.Chat()))
		}
		go func() {
			application.Storages.UploadData(data, photo.Sender().Name())
		}()

	}
	return nil
}

func (application *Application) StorageByConfig(path string) error {
	config, err := config.UnmarshalConfigStorage(path)
	if err != nil {
		return err
	}
	var storage storage.Storage = nil
	switch config.Name {
	case "GoogleDrive":
		storage, err = googledrive.NewByConfig(config)
	}
	if err != nil {
		return err
	}
	if storage == nil {
		logger.LogF("Application: storage %s not found", logger.INFO, config.Name)
		return fmt.Errorf("storage %s not found", config.Name)
	}
	application.Storages = storage
	logger.LogF("Application: added storage %s", logger.INFO, config.Name)
	return nil
}

func (application *Application) MessengerByConfig(path string) error {
	config, err := config.UnmarshalConfigMessenger(path)
	if err != nil {
		return err
	}
	var messenger messenger.Messenger = nil
	switch config.Name {
	case "Telegram":
		messenger, err = telegram.NewByConfig(config)
	}
	if err != nil {
		return err
	}
	if messenger == nil {
		logger.LogF("Application: messenger %s not found", logger.INFO, config.Name)
		return fmt.Errorf("storage %s not found", config.Name)
	}
	application.Messengers = messenger
	logger.LogF("Application: added messenger %s", logger.INFO, config.Name)
	return nil
}

func (application *Application) NotifierByConfig(path string) error {
	configs, err := config.UnmarshalConfigNotifier(path)
	if err != nil {
		return err
	}

	for _, config := range configs {
		var notifier notify.Notifier = nil
		switch config.Name {
		case "Slack":
			notifier = slack2.New(config.Token, config.Channels...)
		}
		if notifier == nil {
			logger.LogF("Application: notifier %s not found", logger.INFO, config.Name)
			return fmt.Errorf("notifier %s not found", config.Name)
		}
		application.Notifiers = append(application.Notifiers, notifier)
		logger.LogF("Application: added notifier %s", logger.INFO, config.Name)
	}
	return nil
}

func (application *Application) SendNotifiers(status string, msg string, keysAndValues ...interface{}) {
	for _, notifier := range application.Notifiers {
		notifier.Send(status, msg, keysAndValues...)
	}
}

func (application *Application) Start(args ...interface{}) {
	// logic for parsing texts
	err := application.Messengers.TextReceive(func(text messenger.Text) {
		err := application.HandleCommands(text)
		if err != nil {
			application.SendNotifiers("ERROR Text", err.Error(), "Text", text.Text(), "Sender", text.Sender().Name(), "Chat", text.Chat().Name())
		}
	})
	if err != nil {
		logger.Log("Application: %s", logger.DEBUG, err.Error())
	}

	// logic for parsing photos
	err = application.Messengers.PhotoReceive(func(photo messenger.Photo) {
		err := application.HandlePhotos(photo)
		if err != nil {
			application.SendNotifiers("ERROR Photo", err.Error(), "Sender", photo.Sender().Name(), "Chat", photo.Chat().Name())
		}
	})
	if err != nil {
		logger.Log("Application: %s", logger.DEBUG, err.Error())
	}

	// start messenger
	application.Messengers.Start(application.DB)
	application.DB.AddMessenger(models.Messenger{
		Name: application.Messengers.GetID(),
	})
}

func main() {
	/* ressources checking
	if err := agent.Listen(agent.Options{}); err != nil {
		panic(err)
	}
	defer agent.Close()
	*/

	// config loading and parsing
	// create application by config
	application, err := NewByConfig(ApplicationConfig)
	if err != nil {
		log.Fatal(err)
	}

	// create messenger by config
	err = application.MessengerByConfig(MessengerConfig)
	if err != nil {
		log.Fatal(err)
	}

	// create storage by config

	err = application.StorageByConfig(StorageConfig)
	if err != nil {
		log.Fatal(err)
	}

	// create notifier by config
	err = application.NotifierByConfig(NotifierConfig)
	if err != nil {
		log.Fatal(err)
	}

	// create face detector - not by config
	face, err := detection.NewFace()
	if err != nil {
		log.Fatal(err)
	}

	application.Detections = face

	// create db - not by config - later by config
	db, err := sqlite.New(DB_PATH, *logger.Global)
	if err != nil {
		log.Fatal(err)
	}
	application.DB = db

	application.Start()

	// TODO: add some monitoring metrics
	for {
	}
}
