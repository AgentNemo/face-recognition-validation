# Face-Recognition-Validation

Face recognition and validation project over telegram with google drive store. **Only a POC. Needs some clean up.**

## possible photo modes

- Detection: Send photo and get results
- Validation: Send photo and let and get the username. Works only if samples were added before.
- Saving: Just save photos to storage
- Samples: Save samples

## possible commands

- /stats            -   return statistic TODO: add admin
- /detect           -   change photo mode to Detection
- /validate         -   change photo mode to Validation
- /save             -   change photo mode to Saving
- /sample           -   change photo mode to Samples
- /download         -   download all photos added from the user
- /detect saving    -   change photo mode to Detection with saving result data to storage

## NOTES

- CGO_ENABLED=1 must be enabled 

## config

### application

````json
{
  "log_level": "DEBUG",
  "log_files": [
    "app.log",
    "db.log"
  ],
  "mode_default": "FaceDetectionSavingData"
}
````

### messenger

````json
{
  "name": "messenger",
  "credentials": "token",
  "permissions": [
    "TextReceive",
    "PhotoReceive",
    "TextSend",
    "PhotoSend",
    "Statistic",
    "OnUserJoined",
    "Reminder",
    "Admin"
  ],
  "admin": "username",
  "welcome_message": "./config/welcome_message.txt",
  "reminder_message": "./config/reminder_message.txt",
  "reminder_interval": "24h"
}
````

### notifier

````json
[
  {
    "name": "notifier",
    "token": "token",
    "channels": ["channel"]
  }
]
````

### storage

````json
{
  "name": "Messenger",
  "credentials": "path_to_credentials",
  "root": "root directory"
}
````

