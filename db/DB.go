package db

import (
	"XYZ/db/models"
	"github.com/joomcode/errorx"
)

var (
	ErrorDB         = errorx.NewNamespace("ErrorDB")
	ErrorDBNotFound = errorx.NewType(ErrorDB, "NotFound")
)

var Handler DB

type DB interface {
	AddMessenger(messenger models.Messenger) (models.Messenger, error)
	AddChat(messenger models.Messenger, chat models.Chat) (models.Chat, error)
	AddUser(messenger models.Messenger, chat models.Chat, user models.User) (models.User, error)
	GetChats(messenger models.Messenger) []models.Chat
	GetUsers(chat models.Chat) []models.User
	GetUser(chat models.Chat, user models.User) (models.User, error)
}
