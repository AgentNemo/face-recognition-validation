package sqlite

import (
	"XYZ/db/models"
	"XYZ/logger"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	gl "gorm.io/gorm/logger"
	"io"
	"log"
	"os"
	"time"
)

type SQLite struct {
	db *gorm.DB
}

func New(DBPath string, loggerInstance logger.Logger) (*SQLite, error) {
	dbg, err := gorm.Open(sqlite.Open(DBPath), &gorm.Config{Logger: newLogger(loggerInstance)})
	if err != nil {
		return nil, err
	}
	dbg.AutoMigrate(&models.Messenger{})
	dbg.AutoMigrate(&models.Chat{})
	dbg.AutoMigrate(&models.User{})
	db := SQLite{dbg.Session(&gorm.Session{FullSaveAssociations: true})}
	return &db, nil
}

func newLogger(loggerInstance logger.Logger) gl.Interface {
	lWriter := io.MultiWriter(os.Stdout)
	file, err := os.Create(loggerInstance.DBPath)
	if err == nil {
		lWriter = io.MultiWriter(file)
	}

	level := gl.Info
	switch loggerInstance.Level {
	case logger.DEBUG:
		level = gl.Info
	case logger.INFO:
		level = gl.Info
	case logger.WARN:
		level = gl.Warn
	case logger.ERROR:
		level = gl.Error
	}

	newLogger := gl.New(
		log.New(lWriter, "\r\n", log.LstdFlags), // io writer
		gl.Config{
			SlowThreshold: time.Second, // Slow SQL threshold
			LogLevel:      level,       // Log level
			Colorful:      false,       // Disable color
		},
	)

	return newLogger
}

func (s *SQLite) AddMessenger(messenger models.Messenger) (models.Messenger, error) {
	var messengerFind models.Messenger
	err := s.db.First(&messengerFind, "name = ?", messenger.Name).Error
	if err == nil {
		logger.LogF("DB: messenger %s already exits", logger.DEBUG, messenger.Name)
		s.db.Model(&messengerFind).Updates(&messenger)
		return messengerFind, nil
	}
	s.db.Create(&messenger)
	logger.LogF("DB: creating a new messenger %s", logger.DEBUG, messenger.Name)
	return messenger, nil
}

func (s *SQLite) AddChat(messenger models.Messenger, chat models.Chat) (models.Chat, error) {
	messenger, _ = s.AddMessenger(messenger)
	var chatFind models.Chat
	err := s.db.First(&chatFind, "name = ? AND messenger_id = ?", chat.Name, messenger.ID).Error
	if err == nil {
		logger.LogF("DB: chat %s in messenger %s already exits", logger.DEBUG, chat.Name, messenger.Name)
		s.db.Model(&chatFind).Updates(&chat)
		return chatFind, err
	}
	s.db.Model(&messenger).Association("chats").Append(&chat)
	logger.LogF("DB: added the chat %s to the messenger %s", logger.DEBUG, chat.Name, messenger.Name)
	return chat, nil
}

func (s *SQLite) AddUser(messenger models.Messenger, chat models.Chat, user models.User) (models.User, error) {
	chat, _ = s.AddChat(messenger, chat)
	var userFind models.User
	err := s.db.First(&userFind, "name = ? AND chat_id = ?", user.Name, chat.ID).Error
	if err == nil {
		logger.LogF("DB: user %s in chat %s already exits", logger.DEBUG, user.Name, chat.Name)
		s.db.Model(&userFind).Updates(&user)
		return userFind, err
	}

	s.db.Model(&chat).Association("Users").Append(&user)

	logger.LogF("DB: created user %s in chat %s", logger.DEBUG, user.Name, chat.Name)
	return user, nil
}

func (s *SQLite) GetChats(messenger models.Messenger) []models.Chat {
	var chats []models.Chat
	err := s.db.First(&messenger, "name = ?", messenger.Name).Error
	if err != nil {
		return chats
	}
	s.db.Model(&messenger).Association("chats").Find(&chats)
	var retChats []models.Chat
	for _, chat := range chats {
		var users []models.User
		s.db.Model(&chat).Association("Users").Find(&users)
		chat.Users = users
		retChats = append(retChats, chat)
	}
	return retChats
}

func (s *SQLite) GetUsers(chat models.Chat) []models.User {
	var users []models.User
	err := s.db.First(&chat, "name = ?", chat.Name).Error
	if err != nil {
		logger.LogF("DB: chat %s does not exits", logger.DEBUG, chat.Name)
		return users
	}
	s.db.Model(&chat).Association("Users").Find(&users)
	return users
}

func (s *SQLite) GetUser(chat models.Chat, user models.User) (models.User, error) {
	var userFind models.User
	var chatFind models.Chat
	err := s.db.First(&chatFind, "name = ? AND reference = ?", chat.Name, chat.UID).Error
	if err != nil {
		return user, nil
	}
	err = s.db.First(&userFind, "name = ? AND chat_id = ?", user.Name, chatFind.ID).Error
	if err != nil {
		return user, nil
	}
	return userFind, nil
}
