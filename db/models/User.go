package models

type User struct {
	ID           int `gorm:"primary_key, AUTO_INCREMENT"`
	ChatID       int
	Name         string
	CountDetect  int // how many photos were detect
	CountStorage int // how many photos were storaged
	CountPhotos  int // how many photos were send
}

/*
// Creates a new entity in the db
func (u User) Create() User {

}

// Updates the entity with the given new object
func (u User) Update(user User) User {

}

// Reads a entity
func (u User) Read() User {

}

// Deletes the entity
func (u User) Delete() {

}

*/
