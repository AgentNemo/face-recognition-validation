package models

type Chat struct {
	ID          int `gorm:"primary_key, AUTO_INCREMENT"`
	UID         string
	Name        string
	MessengerID int
	Users       []User
}

/*
// Creates a new entity in the db
func (c Chat) Create() Chat {

}

// Updates the entity with the given new object
func (c Chat) Update(chat Chat) Chat {

}

// Reads a entity
func (c Chat) Read() Chat {

}

// Deletes the entity
func (c Chat) Delete() {

}


// Returns all Users
func (c Chat) UsersAll() []User {

}

// Returns all User with the given name
func (c Chat) UsersByName(name string) []User {

}

*/
