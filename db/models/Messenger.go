package models

type Messenger struct {
	ID    int `gorm:"primary_key, AUTO_INCREMENT"`
	Name  string
	Chats []Chat
}

/*
// Creates a new entity in the db
func (m Messenger) Create() Messenger {

}

// Updates the entity with the given new object
func (m Messenger) Update(messenger Messenger) Messenger {

}

// Reads a entity
func (m Messenger) Read() Messenger {

}

// Deletes the entity
func (m Messenger) Delete() {

}

// Returns all Chats
func (m Messenger) ChatsAll() []Chat {
	return db.Handler.GetChats(m)
}

// Returns all Chats with the given
func (m Messenger) ChatByName(name string) []Chat {

}

// Returns the Chat with the given uid
func (m Messenger) ChatByUID(uid string) (Chat, error) {

}

*/
