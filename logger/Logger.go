package logger

import (
	"github.com/joomcode/errorx"
	"go.uber.org/zap"
)

var (
	ErrorLogger             = errorx.NewNamespace("ErrorLogger")
	ErrorLoggerInvalidLevel = errorx.NewType(ErrorLogger, "InvalidLevel")
	ErrorLoggerBuild        = errorx.NewType(ErrorLogger, "Build")
)

var Global *Logger = nil

const (
	DEBUG = "DEBUG"
	INFO  = "INFO"
	WARN  = "WARN"
	ERROR = "ERROR"
)

type Logger struct {
	*zap.SugaredLogger
	Level  string
	DBPath string
}

/*
Creates a global logger.
@level logger level
@paths paths for saving logs; if one path is given
@return returns an error if logger could not be initialized
*/
func New(level string, paths ...string) error {
	var config zap.Config
	var err error = nil
	switch level {
	case WARN, ERROR:
		config = zap.NewProductionConfig()
	case DEBUG, INFO:
		config = zap.NewDevelopmentConfig()
	default:
		err = ErrorLoggerInvalidLevel.New("invalid level")
	}
	if err != nil {
		return err
	}

	DBPath := ""
	switch len(paths) {
	case 2:
		DBPath = paths[1]
		fallthrough
	case 1:
		config.OutputPaths = append(config.OutputPaths, paths[0])
	}

	logger, err := config.Build(zap.AddCallerSkip(1))
	if err != nil {
		return ErrorLoggerBuild.WrapWithNoMessage(err)
	}
	Global = &Logger{logger.Sugar(), level, DBPath}
	Log("Logger: logger created", DEBUG)
	return nil
}

/*
Logs a msg
@msg log message
@level log level
@context map[string]interface
*/
func Log(msg string, level string, context ...interface{}) {
	if Global == nil {
		return
	}
	defer Global.Sync()
	switch level {
	case DEBUG:
		Global.Debugw(msg, context...)
	case INFO:
		Global.Infow(msg, context...)
	case WARN:
		Global.Warnw(msg, context...)
	case ERROR:
		Global.Errorw(msg, context...)
	}
}

/*
Logs an error
@err error
@context map[string]interface
*/
func LogE(err error, context ...interface{}) {
	if Global == nil {
		return
	}
	defer Global.Sync()
	Global.Error(err)
}

/*
Logs a msg in the Sprintf style
@msg log message
@level logger level
@a args for building the msg
*/
func LogF(msg string, level string, a ...interface{}) {
	if Global == nil {
		return
	}
	defer Global.Sync()
	switch level {
	case DEBUG:
		Global.Debugf(msg, a...)
	case INFO:
		Global.Infof(msg, a...)
	case WARN:
		Global.Warnf(msg, a...)
	case ERROR:
		Global.Errorf(msg, a...)
	}
}
