package googledrive

import (
	"XYZ/config"
	"XYZ/logger"
	"XYZ/storage"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"sync"
	"time"
)

const (
	TokenPath = "./token.json"
)

type GoogleDrive struct {
	service *drive.Service
	root    *drive.File
	photos  *drive.File
	data    *drive.File
	sync.Mutex
}

/*
Creates a new google drive storage.
@root root directory id, all user directories are being created in this
@credentials google drive credentials parsed as json bytes
@token google acc token as json bytes
*/
func New(root string, credentials []byte, token []byte) (*GoogleDrive, error) {
	gD := &GoogleDrive{}
	service, err := gD.getService(credentials, token)
	if err != nil {
		return nil, err
	}
	gD.service = service
	dirRoot, err := gD.getDir(root, "root")
	if err != nil {
		return nil, err
	}
	gD.root = dirRoot
	dirData, err := gD.getDir(storage.DirData, gD.root.Id)
	if err != nil {
		return nil, err
	}
	gD.data = dirData
	dirPhotos, err := gD.getDir(storage.DirPhotos, gD.root.Id)
	if err != nil {
		return nil, err
	}
	gD.photos = dirPhotos
	return gD, nil
}

func NewByConfig(config config.Storage) (*GoogleDrive, error) {
	credentials, err := ioutil.ReadFile(path.Join(config.Credentials, "credentials.json"))
	if err != nil {
		return nil, err
	}
	token, err := ioutil.ReadFile(path.Join(config.Credentials, "token.json"))
	return New(config.Root, credentials, token)
}

/*
Uploads an image.
@img image as byte arr
@username username
*/
func (g *GoogleDrive) UploadImg(img []byte, username string) error {
	dir, err := g.getDir(username, g.photos.Id)
	if err != nil {
		return storage.ErrorStorageUpload.WrapWithNoMessage(err)
	}
	// create a unique filename
	fileName := fmt.Sprintf("%s-%s", username, time.Now().String())
	_, err = g.createFile(fmt.Sprintf("%s.jpg", fileName), "image/jpeg", bytes.NewReader(img), dir.Id)
	if err != nil {
		return storage.ErrorStorageUpload.WrapWithNoMessage(err)
	}
	logger.LogF("Google Drive: successfully created %s for user %s", logger.DEBUG, fileName, username)
	return nil
}

/*
Uploads a data set.
@data data set as byte arr
@username username
*/
func (g *GoogleDrive) UploadData(data []byte, username string) error {
	dir, err := g.getDir(username, g.data.Id)
	if err != nil {
		return storage.ErrorStorageUpload.WrapWithNoMessage(err)
	}
	// create a unique filename
	fileName := fmt.Sprintf("%s", time.Now().String())
	_, err = g.createFile(fmt.Sprintf("%s.jpg", fileName), "image/jpeg", bytes.NewReader(data), dir.Id)
	if err != nil {
		return storage.ErrorStorageUpload.WrapWithNoMessage(err)
	}
	logger.LogF("Google Drive: successfully created %s for user %s", logger.DEBUG, fileName, username)
	return nil
}

/**
Downloads all images related to a user
@username username
*/
func (g *GoogleDrive) DownloadImg(username string) ([][]byte, error) {
	photos := make([][]byte, 0)
	dir, err := g.getDir(username, g.photos.Id)
	if err != nil {
		return nil, storage.ErrorStorageDownload.WrapWithNoMessage(err)
	}

	files, err := g.getDirContent(dir)
	if err != nil {
		return nil, err
	}

	logger.LogF("Google Drive: found %d photos for download for %s", logger.DEBUG, len(files.Files), username)

	if len(files.Files) < 1 {
		return nil, storage.ErrorStorageEmpty.NewWithNoMessage()
	}

	var wg sync.WaitGroup
	var mutex sync.Mutex
	for _, file := range files.Files {
		wg.Add(1)
		go func(wg *sync.WaitGroup, id string) {
			defer wg.Done()
			res, err := g.service.Files.Get(id).Download()
			if err != nil {
				logger.LogF("Google Drive: error while downloading photo %s for %s", logger.DEBUG, file.Name, username)
				return
			}
			fileRaw, err := ioutil.ReadAll(res.Body)
			if err != nil {
				logger.LogF("Google Drive: error while reading photo %s for %s", logger.DEBUG, file.Name, username)
				return
			}
			logger.LogF("Google Drive: successfully downloaded photo %s for %s", logger.DEBUG, file.Name, username)
			mutex.Lock()
			photos = append(photos, fileRaw)
			mutex.Unlock()
		}(&wg, file.Id)
	}
	wg.Wait()
	logger.LogF("Google Drive: successfully downloaded %d photos", logger.DEBUG, len(photos))
	return photos, nil
}

/**
Downloads all data sets related to a user
@username username
*/
func (g *GoogleDrive) DownloadData(username string) ([][]byte, error) {
	data := make([][]byte, 0)
	dir, err := g.getDir(username, g.data.Id)
	if err != nil {
		return nil, storage.ErrorStorageDownload.WrapWithNoMessage(err)
	}

	files, err := g.getDirContent(dir)
	if err != nil {
		return nil, err
	}

	logger.LogF("Google Drive: found %d data sets for download for %s", logger.DEBUG, len(files.Files), username)
	if len(files.Files) < 1 {
		return nil, storage.ErrorStorageEmpty.NewWithNoMessage()
	}

	var wg sync.WaitGroup
	var mutex sync.Mutex
	for _, file := range files.Files {
		wg.Add(1)
		go func(wg *sync.WaitGroup, id string) {
			defer wg.Done()
			res, err := g.service.Files.Get(id).Download()
			if err != nil {
				logger.LogF("Google Drive: error while downloading data set %s for %s", logger.DEBUG, file.Name, username)
				return
			}
			fileRaw, err := ioutil.ReadAll(res.Body)
			if err != nil {
				logger.LogF("Google Drive: error while reading data set %s for %s", logger.DEBUG, file.Name, username)
				return
			}
			logger.LogF("Google Drive: successfully downloaded data set %s for %s", logger.DEBUG, file.Name, username)
			mutex.Lock()
			data = append(data, fileRaw)
			mutex.Unlock()
		}(&wg, file.Id)
	}
	wg.Wait()
	logger.LogF("Google Drive: successfully downloaded %d data sets for %s", logger.DEBUG, len(data), username)
	return data, nil
}

/*
Downloads all data sets
*/
func (g *GoogleDrive) DownloadDataAll() (map[string][][]byte, error) {
	dataSets := make(map[string][][]byte, 0)
	dir, err := g.getDir(g.data.Name, g.root.Id)
	if err != nil {
		return nil, storage.ErrorStorageDownload.WrapWithNoMessage(err)
	}

	files, err := g.getDirContent(dir)
	if err != nil {
		return nil, err
	}

	logger.LogF("Google Drive: found %d users in %s", logger.DEBUG, len(files.Files), g.data.Name)

	if len(files.Files) < 1 {
		return nil, storage.ErrorStorageEmpty.NewWithNoMessage()
	}

	var wg sync.WaitGroup
	var mutex sync.Mutex
	for _, file := range files.Files {
		wg.Add(1)
		go func(wg *sync.WaitGroup, user string) {
			data, err := g.DownloadData(user)
			if err != nil {
				return
			}
			mutex.Lock()
			dataSets[file.Name] = data
			mutex.Unlock()
			wg.Done()
		}(&wg, file.Name)
	}
	wg.Wait()
	return dataSets, nil
}

/*
Downloads all images
*/
func (g *GoogleDrive) DownloadImgAll() (map[string][][]byte, error) {
	photos := make(map[string][][]byte, 0)
	dir, err := g.getDir(g.photos.Name, g.root.Id)
	if err != nil {
		return nil, storage.ErrorStorageDownload.WrapWithNoMessage(err)
	}

	files, err := g.getDirContent(dir)
	if err != nil {
		return nil, err
	}

	logger.LogF("Google Drive: found %d users in %s", logger.DEBUG, len(files.Files), g.photos.Name)

	if len(files.Files) < 1 {
		return nil, storage.ErrorStorageEmpty.NewWithNoMessage()
	}

	var wg sync.WaitGroup
	var mutex sync.Mutex
	for _, file := range files.Files {
		wg.Add(1)
		go func(wg *sync.WaitGroup, user string) {
			data, err := g.DownloadImg(user)
			if err != nil {
				return
			}
			mutex.Lock()
			photos[file.Name] = data
			mutex.Unlock()
			wg.Done()
		}(&wg, file.Name)
	}

	wg.Wait()
	return photos, nil
}

func (g *GoogleDrive) StatisticImg(username string) int {
	dir, err := g.getDir(username, g.photos.Id)
	if err != nil {
		return -1
	}

	files, err := g.getDirContent(dir)
	if err != nil {
		return -1
	}
	return len(files.Files)
}

func (g *GoogleDrive) StatisticData(username string) int {
	dir, err := g.getDir(username, g.data.Id)
	if err != nil {
		return -1
	}
	files, err := g.getDirContent(dir)
	if err != nil {
		return -1
	}
	return len(files.Files)
}

func (g *GoogleDrive) StatisticImgAll() map[string]int {
	imgData := make(map[string]int, 0)
	dir, err := g.getDir(g.photos.Name, g.root.Id)
	if err != nil {
		logger.LogF("%s", logger.WARN, err.Error())
		return imgData
	}
	files, err := g.getDirContent(dir)
	if err != nil {
		logger.LogF("%s", logger.WARN, err.Error())
		return imgData
	}

	logger.LogF("Google Drive: found %d users in %s", logger.DEBUG, len(files.Files), g.photos.Name)

	for _, file := range files.Files {
		imgData[file.Name] = g.StatisticImg(file.Name)
	}
	return imgData
}

func (g *GoogleDrive) StatisticDataAll() map[string]int {
	dataData := make(map[string]int, 0)
	dir, err := g.getDir(g.data.Name, g.root.Id)
	if err != nil {
		logger.LogF("%s", logger.WARN, err.Error())
		return dataData
	}
	files, err := g.getDirContent(dir)
	if err != nil {
		logger.LogF("%s", logger.WARN, err.Error())
		return dataData
	}

	logger.LogF("Google Drive: found %d users in %s", logger.DEBUG, len(files.Files), g.data.Name)

	for _, file := range files.Files {
		dataData[file.Name] = g.StatisticData(file.Name)
	}
	return dataData
}

/*
Creates a new directory.
@name directory name
@parentID directory id in which the new directory should be created
*/
func (g *GoogleDrive) createDir(name string, parentID string) (*drive.File, error) {
	d := &drive.File{
		Name:     name,
		MimeType: "application/vnd.google-apps.folder",
		Parents:  []string{parentID},
	}
	file, err := g.service.Files.Create(d).Do()
	if err != nil {
		logger.LogF("Google Drive: could not create directory %s in %s", logger.DEBUG, name, parentID)
		return nil, storage.ErrorStorageDir.WrapWithNoMessage(err)
	}
	return file, nil
}

/*
Returns the content of a directory
@dir directory
*/
func (g *GoogleDrive) getDirContent(dir *drive.File) (*drive.FileList, error) {
	files, err := g.service.Files.List().Q(fmt.Sprintf("'%s' in parents and trashed=false", dir.Id)).Do()
	if err != nil {
		logger.LogF("Google Drive: fetching content in %s failed", logger.DEBUG, dir.Name)
		return nil, storage.ErrorStorageOther.WrapWithNoMessage(err)
	}
	return files, nil
}

/*
Fetches a directory. If directory does not exits a new one is created
@name directory name
@parentID directory id of the parent
*/
func (g *GoogleDrive) getDir(name string, parentID string) (*drive.File, error) {
	g.Lock()
	defer g.Unlock()
	dirList, err := g.service.Files.List().Q(fmt.Sprintf("mimeType='application/vnd.google-apps.folder' and trashed=false and name='%s' and '%s' in parents", name, parentID)).Do()
	if err != nil {
		return nil, storage.ErrorStorageDir.WrapWithNoMessage(err)
	}
	for _, dir := range dirList.Files {
		if dir.Name == name {
			return dir, nil
		}
	}
	return g.createDir(name, parentID)
}

/**
Creates a new file
@name file name
@mimeType file type
@content file content
@parentID directory id in which the file should be saved
*/
func (g *GoogleDrive) createFile(name string, mimeType string, content io.Reader, parentID string) (*drive.File, error) {
	f := &drive.File{
		MimeType: mimeType,
		Name:     name,
		Parents:  []string{parentID},
	}
	file, err := g.service.Files.Create(f).Media(content).Do()

	if err != nil {
		logger.LogF("Google Drive: file %s of type %s could not be created", logger.DEBUG, name, mimeType)
		return nil, storage.ErrorStorageFile.WrapWithNoMessage(err)
	}
	return file, nil
}

/*
Creates a service instance.
*/
func (g *GoogleDrive) getService(credentials []byte, token []byte) (*drive.Service, error) {

	config, err := google.ConfigFromJSON(credentials, drive.DriveScope)
	if err != nil {
		logger.Log("Google Drive: config can not be read", logger.WARN)
		return nil, storage.ErrorStorageCredentials.WrapWithNoMessage(err)
	}

	client, err := getClient(config, token)
	if err != nil {
		logger.Log("Google Drive: client could not be created", logger.WARN)
		return nil, err
	}
	service, err := drive.New(client)
	if err != nil {
		logger.Log("Google Drive: service can not be created", logger.WARN)
		return nil, storage.ErrorStorageOther.WrapWithNoMessage(err)
	}

	return service, err
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config, token []byte) (*http.Client, error) {
	tok, err := tokenFromBytes(token)
	if err != nil {
		logger.Log("Google Drive: no valid token", logger.INFO)
		tok = getTokenFromWeb(config)
		if tok == nil {
			logger.Log("Google drive: unable to get token from web", logger.ERROR)
			return nil, storage.ErrorStorageCredentials.NewWithNoMessage()
		}
		saveToken(TokenPath, tok)
	}
	return config.Client(context.Background(), tok), nil
}

// Retrieves a token from a byte arr
func tokenFromBytes(token []byte) (*oauth2.Token, error) {
	if token == nil {
		return nil, storage.ErrorStorageCredentials.NewWithNoMessage()
	}
	tok := &oauth2.Token{}
	err := json.NewDecoder(bytes.NewReader(token)).Decode(tok)
	return tok, err
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		logger.LogF("Google drive: Unable to read authorization code %v", logger.WARN, err)
		return nil
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		logger.LogF("Google Drive: Unable to retrieve token from web %v", logger.WARN, err)
		return nil
	}
	return tok
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
