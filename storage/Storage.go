package storage

import (
	"github.com/joomcode/errorx"
)

const (
	DirPhotos = "photos"
	DirData   = "data"
)

var (
	ErrorStorage            = errorx.NewNamespace("ErrorStorage")
	ErrorStorageCredentials = errorx.NewType(ErrorStorage, "Credentials")
	ErrorStorageUpload      = errorx.NewType(ErrorStorage, "Upload")
	ErrorStorageDownload    = errorx.NewType(ErrorStorage, "Download")
	ErrorStorageOther       = errorx.NewType(ErrorStorage, "Other")
	ErrorStorageDir         = errorx.NewType(ErrorStorage, "Directory")
	ErrorStorageFile        = errorx.NewType(ErrorStorage, "File")
	ErrorStorageEmpty       = errorx.NewType(ErrorStorage, "Empty")
)

type Storage interface {
	UploadData(data []byte, username string) error  // Uploads a data set related to the username
	UploadImg(img []byte, username string) error    // Upload an image related to the username
	DownloadData(username string) ([][]byte, error) // Downloads all data related to the username
	DownloadImg(username string) ([][]byte, error)  // Downloads all data related to the username
	DownloadDataAll() (map[string][][]byte, error)  // Downloads all data for all users
	DownloadImgAll() (map[string][][]byte, error)   // Downloads all img for all users
	StatisticImg(username string) int               // Returns the number of images saved for a user
	StatisticData(username string) int              // Returns the number of data sets saved for a user
	StatisticImgAll() map[string]int                // Returns the number of images saved for all users
	StatisticDataAll() map[string]int               // Returns the number of data sets saved for all users
}
