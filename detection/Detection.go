package detection

import "github.com/joomcode/errorx"

var (
	ErrorDetection        = errorx.NewNamespace("Detections")
	ErrorDetectionNoUser  = errorx.NewType(ErrorDetection, "NoUser")
	ErrorDetectionNoMatch = errorx.NewType(ErrorDetection, "NoMatch")
	ErrorDetectionFormat  = errorx.NewType(ErrorDetection, "Format")
	ErrorDetectionNoData  = errorx.NewType(ErrorDetection, "NoData")
)

type Detection interface {
	Detect(img []byte) ([][]byte, error)           // parses from the image some sub images
	Recognize(img []byte) ([]string, error)        // validates an img and returns the username
	Apply()                                        // applies the samples
	AddSamples(username string, data []byte) error // sets sample for a specific user
	Raw(img []byte) ([]byte, error)                // detects and returns the results as raw
}
