package detection

import (
	"XYZ/logger"
	"bytes"
	"encoding/gob"
	"github.com/Kagami/go-face"
	"hash/fnv"
	"image"
	"image/jpeg"
)

const (
	modelsDir = "./detection/data/models"
)

type Face struct {
	recognizer *face.Recognizer
	samples    map[string][]face.Descriptor
	users      map[int]string
}

func NewFace() (*Face, error) {
	fc := Face{users: make(map[int]string), samples: make(map[string][]face.Descriptor)}
	rec, err := face.NewRecognizer(modelsDir)
	if err != nil {
		return nil, err
	}
	fc.recognizer = rec
	return &fc, nil
}

/*
Detects faces in an img and returns every subimage
*/
func (f *Face) Detect(img []byte) ([][]byte, error) {
	detections := make([][]byte, 0)
	facesRaw, err := f.Raw(img)
	if err != nil {
		return nil, err
	}
	src, _ := jpeg.Decode(bytes.NewReader(img))
	var faces []face.Face
	err = GetInterface(facesRaw, &faces)
	if err != nil {
		return nil, ErrorDetectionFormat.WrapWithNoMessage(err)
	}
	for _, face := range faces {
		newsrc := src.(interface {
			SubImage(r image.Rectangle) image.Image
		}).SubImage(face.Rectangle)

		buf := new(bytes.Buffer)

		err = jpeg.Encode(buf, newsrc, nil)
		if err == nil {
			detections = append(detections, buf.Bytes())
		}
	}
	logger.LogF("Face: parsed %d imgs", logger.DEBUG, len(detections))
	return detections, nil
}

/*
Classifies and validates an img
*/
func (f *Face) Recognize(img []byte) ([]string, error) {
	users := make([]string, 0)
	faces, err := f.recognizer.Recognize(img)
	if err != nil {
		logger.Log("Face: no recognition found in validation img", logger.DEBUG)
		return nil, ErrorDetectionNoMatch.NewWithNoMessage()
	}
	for _, face := range faces {
		id := f.recognizer.Classify(face.Descriptor)
		if id < 0 {
			logger.Log("Face: classification failed", logger.DEBUG)
			continue
		}
		users = append(users, f.users[id])
	}
	return users, nil
}

func (f *Face) Apply() {
	var samples []face.Descriptor
	var cats []int32
	for username, discriptors := range f.samples {
		id := hash(username)
		for _, discriptor := range discriptors {
			samples = append(samples, discriptor)
			cats = append(cats, id)
		}
		f.users[int(id)] = username
	}
	f.recognizer.SetSamples(samples, cats)
}

func (f *Face) AddSamples(username string, data []byte) error {
	var faces []face.Face
	err := GetInterface(data, &faces)
	if err != nil {
		return ErrorDetectionFormat.WrapWithNoMessage(err)
	}
	for _, face := range faces {
		f.samples[username] = append(f.samples[username], face.Descriptor)
	}
	return nil
}

func (f *Face) Raw(img []byte) ([]byte, error) {
	_, err := jpeg.Decode(bytes.NewReader(img))
	if err != nil {
		logger.Log("Face: wrong format", logger.DEBUG)
		return nil, ErrorDetectionFormat.NewWithNoMessage()
	}
	faces, err := f.recognizer.Recognize(img)
	logger.LogF("Face: found %d faces", logger.DEBUG, len(faces))
	if err != nil {
		return nil, err
	}
	return GetBytes(faces)
}

func GetBytes(key interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(key)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func GetInterface(data []byte, key *[]face.Face) error {
	dec := gob.NewDecoder(bytes.NewReader(data))
	return dec.Decode(key)
}

func hash(s string) int32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return int32(h.Sum32())
}
